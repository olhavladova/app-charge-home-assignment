# Bundle Component

This is a home assignment project which includes a Bundle component and its child components. The project has been implemented using Angular and TypeScript, and follows best practices for writing clean and efficient code.

The Bundle component (consisting of BannerComponent as the container, and other components as presentational components) is generic and can render different values passed through its properties. The project uses `DataService` to asynchronously fetch the bundle data, simulating varying network conditions with a random delay.

## Project Structure

The project consists of the following main components:

- `BannerComponent` (Container): This is the main component that displays the banner with all the bundle details. It fetches the bundle data from a service and passes it down to the child components.

The presentational components are:

- `TopBannerComponent`: Displays the top part of the banner, including the background image and the product artifacts.
- `BottomBannerComponent`: Displays the bottom part of the banner, including the availability, price, and timer.
- `BannerArtifactComponent`: Represents a single product artifact in the banner.

Shared components are located in a shared module:

- `ButtonComponent`: A reusable button component that is used for the purchase button in `BottomBannerComponent`.
- `LoaderComponent`: A reusable component used to display a loading state across the application.

The styles of the components have been written in SCSS, and utilize functions, variables, and mixins to ensure reusability and maintainability. Media queries are used to ensure the responsiveness of the design.

## SharedModule

SharedModule includes:

- `ButtonComponent` and `LoaderComponent`: Reusable components used across the application.
- `DataService`: A service responsible for fetching the bundle data. It uses an Observable that emits new bundles with a random delay to simulate varying network conditions. The service also handles any errors that may occur during data fetching, emitting `null` in such cases.
- `CountdownPipe`: A custom Angular pipe to transform bundle time into a countdown format.
- `Models`: TypeScript interfaces and types used across the application.

## Installation

To run this project locally, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/olhavladova/app-charge-home-assignment.git`
2. Navigate into the project directory
3. Install the dependencies: `npm install`
4. Run the project: `npm start`
5. Open your browser and navigate to `http://localhost:4200`

## Styling

The application uses SCSS for styling and follows a modular approach. Each component has its own SCSS file which makes it easier to maintain and update styles. The use of SCSS functions, variables, and mixins ensure consistency across the application. Media queries have been used to ensure that the design is responsive and works across different screen sizes.
