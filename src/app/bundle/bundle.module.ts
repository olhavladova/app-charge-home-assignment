import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { BannerComponent } from './containers/banner/banner.component';
import { SharedModule } from '../shared/shared.module';
import { TopBannerComponent } from './components/top-banner/top-banner.component';
import { BottomBannerComponent } from './components/bottom-banner/bottom-banner.component';
import { BannerArtifactComponent } from './components/banner-artifact/banner-artifact.component';

@NgModule({
	declarations: [
		BannerComponent,
		TopBannerComponent,
		BottomBannerComponent,
		BannerArtifactComponent,
	],
	imports: [CommonModule, SharedModule, NgOptimizedImage],
	exports: [BannerComponent],
})
export class BundleModule {}
