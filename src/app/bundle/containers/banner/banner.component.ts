import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { DataService } from '../../../shared/services';
import { Bundle } from '../../../shared/models';

@Component({
	selector: 'app-banner',
	templateUrl: './banner.component.html',
	styleUrls: ['./banner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerComponent {
	bundle$: Observable<Bundle | null>;

	constructor(private dataService: DataService) {
		this.bundle$ = timer(0, 2000).pipe(
			concatMap(() => this.dataService.getBundle())
		);
	}
}
