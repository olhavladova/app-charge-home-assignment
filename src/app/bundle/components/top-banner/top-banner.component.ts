import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Bundle } from '../../../shared/models';

@Component({
	selector: 'app-top-banner',
	templateUrl: './top-banner.component.html',
	styleUrls: ['./top-banner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBannerComponent {
	@Input() bundle!: Bundle;
}
