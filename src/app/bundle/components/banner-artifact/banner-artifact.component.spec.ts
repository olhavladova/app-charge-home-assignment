import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerArtifactComponent } from './banner-artifact.component';

describe('BannerArtifactComponent', () => {
  let component: BannerArtifactComponent;
  let fixture: ComponentFixture<BannerArtifactComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BannerArtifactComponent]
    });
    fixture = TestBed.createComponent(BannerArtifactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
