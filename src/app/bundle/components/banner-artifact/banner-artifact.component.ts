import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Product } from '../../../shared/models';

@Component({
	selector: 'app-banner-artifact',
	templateUrl: './banner-artifact.component.html',
	styleUrls: ['./banner-artifact.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerArtifactComponent {
	@Input() product!: Product;
}
