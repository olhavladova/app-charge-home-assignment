import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Bundle } from '../../../shared/models';

@Component({
	selector: 'app-bottom-banner',
	templateUrl: './bottom-banner.component.html',
	styleUrls: ['./bottom-banner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BottomBannerComponent {
	@Input() bundle!: Bundle;
	buyBundle(): void {
		console.log('The purchase is made!');
	}
}
