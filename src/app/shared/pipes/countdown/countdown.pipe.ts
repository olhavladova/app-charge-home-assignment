import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'countdown',
})
export class CountdownPipe implements PipeTransform {
	transform(time: number): string {
		const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
		const minutes = Math.floor((time / (1000 * 60)) % 60);
		const seconds = Math.floor((time / 1000) % 60);
		return `${this.padZero(hours)}:${this.padZero(minutes)}:${this.padZero(
			seconds
		)}`;
	}

	private padZero(value: number): string {
		return value.toString().padStart(2, '0');
	}
}
