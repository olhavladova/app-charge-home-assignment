import { Injectable } from '@angular/core';
import { catchError, delay, Observable, of } from 'rxjs';
import { Bundle } from '../models';

@Injectable({
	providedIn: 'root',
})
export class DataService {
	private bundles: Bundle[] = [
		{
			bgImage: '../../assets/images/cover.png',
			timeLeftMs: 20000,
			maxAvailable: 10,
			available: 5,
			price: {
				amount: 99,
				currencySymbol: '$',
			},
			products: [
				{
					productImage: 'assets/images/asset2.png',
					amount: '6,000',
					productTextColor: 'teal',
				},
				{
					productImage: 'assets/images/asset3.png',
					amount: '30M',
					productTextColor: 'gold',
				},
				{
					productImage: 'assets/images/asset1.png',
					amount: '20',
					productTextColor: 'purple',
				},
			],
		},
		{
			bgImage: '../../assets/images/cover.png',
			timeLeftMs: 7200000,
			maxAvailable: 20,
			available: 15,
			price: {
				amount: 149,
				currencySymbol: '$',
			},
			products: [
				{
					productImage: 'assets/images/asset2.png',
					amount: '12,000',
					productTextColor: 'blue',
				},
				{
					productImage: 'assets/images/asset3.png',
					amount: '60M',
					productTextColor: 'gold',
				},
				{
					productImage: 'assets/images/asset1.png',
					amount: '40',
					productTextColor: 'pink',
				},
			],
		},
		{
			bgImage: '../../assets/images/cover.png',
			timeLeftMs: 10800000,
			maxAvailable: 5,
			available: 2,
			price: {
				amount: 199,
				currencySymbol: '$',
			},
			products: [
				{
					productImage: 'assets/images/asset2.png',
					amount: '18,000',
					productTextColor: 'green',
				},
				{
					productImage: 'assets/images/asset3.png',
					amount: '100M',
					productTextColor: 'gold',
				},
				{
					productImage: 'assets/images/asset1.png',
					amount: '60',
					productTextColor: 'red',
				},
			],
		},
	];
	private currentBundleIndex = 0;

	constructor() {}

	getBundle(): Observable<Bundle | null> {
		const bundle: Bundle = this.bundles[this.currentBundleIndex];
		this.currentBundleIndex =
			(this.currentBundleIndex + 1) % this.bundles.length;

		// Random delay between 1s and 3s
		const delayTime = Math.floor(Math.random() * 2000) + 1000;

		return of(bundle).pipe(
			delay(delayTime),
			catchError(error => {
				console.error(`Error fetching bundle: ${error}`);
				return of(null); // On error, emit null
			})
		);
	}
}
