import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { CountdownPipe } from './pipes/countdown/countdown.pipe';
import { ButtonComponent } from './components/button/button.component';

@NgModule({
	declarations: [LoaderComponent, CountdownPipe, ButtonComponent],
	exports: [LoaderComponent, CountdownPipe, ButtonComponent],
	imports: [CommonModule],
})
export class SharedModule {}
