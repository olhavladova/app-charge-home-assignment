export interface Product {
	productImage: string;
	amount: string;
	productTextColor: string;
}
