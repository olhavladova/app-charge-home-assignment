import { Product } from './product.interface';

export interface Bundle {
	bgImage: string;
	timeLeftMs: number;
	maxAvailable: number;
	available: number;
	price: {
		amount: number;
		currencySymbol: string;
	};
	products: Product[];
}
