import {
	Component,
	Input,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
	@Input() label!: string;
	@Input() color: string =
		'linear-gradient(-5deg, rgb(42, 131, 10) 14%, rgb(138, 216, 71) 100%)';
	@Output() onClick = new EventEmitter<void>();

	onClickButton(): void {
		this.onClick.emit();
	}
}
